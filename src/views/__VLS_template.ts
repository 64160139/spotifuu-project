import { mdiAccount, mdiLock } from '@mdi/js';
import { __VLS_internalComponent, __VLS_componentsOption, __VLS_name } from './HomeView.vue';

function __VLS_template() {
let __VLS_ctx!: InstanceType<__VLS_PickNotAny<typeof __VLS_internalComponent, new () => {}>> & {};
/* Components */
let __VLS_otherComponents!: NonNullable<typeof __VLS_internalComponent extends { components: infer C; } ? C : {}> & typeof __VLS_componentsOption;
let __VLS_own!: __VLS_SelfComponent<typeof __VLS_name, typeof __VLS_internalComponent & (new () => { $slots: typeof __VLS_slots; })>;
let __VLS_localComponents!: typeof __VLS_otherComponents & Omit<typeof __VLS_own, keyof typeof __VLS_otherComponents>;
let __VLS_components!: typeof __VLS_localComponents & __VLS_GlobalComponents & typeof __VLS_ctx;
/* Style Scoped */
type __VLS_StyleScopedClasses = {};
let __VLS_styleScopedClasses!: __VLS_StyleScopedClasses | keyof __VLS_StyleScopedClasses | (keyof __VLS_StyleScopedClasses)[];
/* CSS variable injection */
/* CSS variable injection end */
let __VLS_resolvedLocalAndGlobalComponents!: {} &
__VLS_WithComponent<'VContainer', typeof __VLS_localComponents, "VContainer", "vContainer", "v-container"> &
__VLS_WithComponent<'VParallax', typeof __VLS_localComponents, "VParallax", "vParallax", "v-parallax"> &
__VLS_WithComponent<'VRow', typeof __VLS_localComponents, "VRow", "vRow", "v-row"> &
__VLS_WithComponent<'VCol', typeof __VLS_localComponents, "VCol", "vCol", "v-col"> &
__VLS_WithComponent<'VCard', typeof __VLS_localComponents, "VCard", "vCard", "v-card"> &
__VLS_WithComponent<'VCardTitle', typeof __VLS_localComponents, "VCardTitle", "vCardTitle", "v-card-title"> &
__VLS_WithComponent<'VCardText', typeof __VLS_localComponents, "VCardText", "vCardText", "v-card-text"> &
__VLS_WithComponent<'VForm', typeof __VLS_localComponents, "VForm", "vForm", "v-form"> &
__VLS_WithComponent<'VTextField', typeof __VLS_localComponents, "VTextField", "vTextField", "v-text-field"> &
__VLS_WithComponent<'VCardActions', typeof __VLS_localComponents, "VCardActions", "vCardActions", "v-card-actions"> &
__VLS_WithComponent<'VBtn', typeof __VLS_localComponents, "VBtn", "vBtn", "v-btn"> &
__VLS_WithComponent<'VSpacer', typeof __VLS_localComponents, "VSpacer", "vSpacer", "v-spacer">;
__VLS_intrinsicElements.div; __VLS_intrinsicElements.div;
__VLS_components.VContainer; __VLS_components.VContainer; __VLS_components.vContainer; __VLS_components.vContainer; __VLS_components["v-container"]; __VLS_components["v-container"];
// @ts-ignore
[VContainer, VContainer,];
__VLS_components.VParallax; __VLS_components.VParallax; __VLS_components.vParallax; __VLS_components.vParallax; __VLS_components["v-parallax"]; __VLS_components["v-parallax"];
// @ts-ignore
[VParallax, VParallax,];
__VLS_components.VRow; __VLS_components.VRow; __VLS_components.vRow; __VLS_components.vRow; __VLS_components["v-row"]; __VLS_components["v-row"];
// @ts-ignore
[VRow, VRow,];
__VLS_components.VCol; __VLS_components.VCol; __VLS_components.VCol; __VLS_components.vCol; __VLS_components.vCol; __VLS_components.vCol; __VLS_components["v-col"]; __VLS_components["v-col"]; __VLS_components["v-col"];
// @ts-ignore
[VCol, VCol, VCol,];
__VLS_components.VCard; __VLS_components.VCard; __VLS_components.VCard; __VLS_components.VCard; __VLS_components.vCard; __VLS_components.vCard; __VLS_components.vCard; __VLS_components.vCard; __VLS_components["v-card"]; __VLS_components["v-card"]; __VLS_components["v-card"]; __VLS_components["v-card"];
// @ts-ignore
[VCard, VCard, VCard, VCard,];
__VLS_components.VCardTitle; __VLS_components.VCardTitle; __VLS_components.vCardTitle; __VLS_components.vCardTitle; __VLS_components["v-card-title"]; __VLS_components["v-card-title"];
// @ts-ignore
[VCardTitle, VCardTitle,];
__VLS_intrinsicElements.h5; __VLS_intrinsicElements.h5;
__VLS_components.VCardText; __VLS_components.VCardText; __VLS_components.VCardText; __VLS_components.VCardText; __VLS_components.vCardText; __VLS_components.vCardText; __VLS_components.vCardText; __VLS_components.vCardText; __VLS_components["v-card-text"]; __VLS_components["v-card-text"]; __VLS_components["v-card-text"]; __VLS_components["v-card-text"];
// @ts-ignore
[VCardText, VCardText, VCardText, VCardText,];
__VLS_components.VForm; __VLS_components.VForm; __VLS_components.vForm; __VLS_components.vForm; __VLS_components["v-form"]; __VLS_components["v-form"];
// @ts-ignore
[VForm, VForm,];
__VLS_components.VTextField; __VLS_components.VTextField; __VLS_components.VTextField; __VLS_components.VTextField; __VLS_components.vTextField; __VLS_components.vTextField; __VLS_components.vTextField; __VLS_components.vTextField; __VLS_components["v-text-field"]; __VLS_components["v-text-field"]; __VLS_components["v-text-field"]; __VLS_components["v-text-field"];
// @ts-ignore
[VTextField, VTextField, VTextField, VTextField,];
__VLS_components.VCardActions; __VLS_components.VCardActions; __VLS_components.VCardActions; __VLS_components.VCardActions; __VLS_components.vCardActions; __VLS_components.vCardActions; __VLS_components.vCardActions; __VLS_components.vCardActions; __VLS_components["v-card-actions"]; __VLS_components["v-card-actions"]; __VLS_components["v-card-actions"]; __VLS_components["v-card-actions"];
// @ts-ignore
[VCardActions, VCardActions, VCardActions, VCardActions,];
__VLS_components.VBtn; __VLS_components.VBtn; __VLS_components.VBtn; __VLS_components.VBtn; __VLS_components.VBtn; __VLS_components.VBtn; __VLS_components.VBtn; __VLS_components.VBtn; __VLS_components.vBtn; __VLS_components.vBtn; __VLS_components.vBtn; __VLS_components.vBtn; __VLS_components.vBtn; __VLS_components.vBtn; __VLS_components.vBtn; __VLS_components.vBtn; __VLS_components["v-btn"]; __VLS_components["v-btn"]; __VLS_components["v-btn"]; __VLS_components["v-btn"]; __VLS_components["v-btn"]; __VLS_components["v-btn"]; __VLS_components["v-btn"]; __VLS_components["v-btn"];
// @ts-ignore
[VBtn, VBtn, VBtn, VBtn, VBtn, VBtn, VBtn, VBtn,];
__VLS_intrinsicElements.template; __VLS_intrinsicElements.template; __VLS_intrinsicElements.template; __VLS_intrinsicElements.template;
__VLS_components.VSpacer; __VLS_components.VSpacer; __VLS_components.vSpacer; __VLS_components.vSpacer; __VLS_components["v-spacer"]; __VLS_components["v-spacer"];
// @ts-ignore
[VSpacer, VSpacer,];
{
const __VLS_0 = __VLS_intrinsicElements["div"];
const __VLS_1 = __VLS_elementAsFunctionalComponent(__VLS_0);
const __VLS_2 = __VLS_1({ ...{}, id: ("app"), }, ...__VLS_functionalComponentArgsRest(__VLS_1));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_0, typeof __VLS_2> & Record<string, unknown>) => void)({ ...{}, id: ("app"), });
const __VLS_3 = __VLS_pickFunctionalComponentCtx(__VLS_0, __VLS_2)!;
let __VLS_4!: __VLS_NormalizeEmits<typeof __VLS_3.emit>;
{
const __VLS_5 = ({} as 'VContainer' extends keyof typeof __VLS_ctx ? { 'VContainer': typeof __VLS_ctx.VContainer; } : 'vContainer' extends keyof typeof __VLS_ctx ? { 'VContainer': typeof __VLS_ctx.vContainer; } : 'v-container' extends keyof typeof __VLS_ctx ? { 'VContainer': (typeof __VLS_ctx)["v-container"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VContainer;
const __VLS_6 = __VLS_asFunctionalComponent(__VLS_5, new __VLS_5({ ...{}, class: ("custom-background mx-auto"), fluid: (true), }));
({} as { VContainer: typeof __VLS_5; }).VContainer;
({} as { VContainer: typeof __VLS_5; }).VContainer;
const __VLS_7 = __VLS_6({ ...{}, class: ("custom-background mx-auto"), fluid: (true), }, ...__VLS_functionalComponentArgsRest(__VLS_6));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_5, typeof __VLS_7> & Record<string, unknown>) => void)({ ...{}, class: ("custom-background mx-auto"), fluid: (true), });
const __VLS_8 = __VLS_pickFunctionalComponentCtx(__VLS_5, __VLS_7)!;
let __VLS_9!: __VLS_NormalizeEmits<typeof __VLS_8.emit>;
{
const __VLS_10 = ({} as 'VParallax' extends keyof typeof __VLS_ctx ? { 'VParallax': typeof __VLS_ctx.VParallax; } : 'vParallax' extends keyof typeof __VLS_ctx ? { 'VParallax': typeof __VLS_ctx.vParallax; } : 'v-parallax' extends keyof typeof __VLS_ctx ? { 'VParallax': (typeof __VLS_ctx)["v-parallax"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VParallax;
const __VLS_11 = __VLS_asFunctionalComponent(__VLS_10, new __VLS_10({ ...{}, src: ("https://cdn.vuetifyjs.com/images/backgrounds/vbanner.jpg"), }));
({} as { VParallax: typeof __VLS_10; }).VParallax;
({} as { VParallax: typeof __VLS_10; }).VParallax;
const __VLS_12 = __VLS_11({ ...{}, src: ("https://cdn.vuetifyjs.com/images/backgrounds/vbanner.jpg"), }, ...__VLS_functionalComponentArgsRest(__VLS_11));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_10, typeof __VLS_12> & Record<string, unknown>) => void)({ ...{}, src: ("https://cdn.vuetifyjs.com/images/backgrounds/vbanner.jpg"), });
const __VLS_13 = __VLS_pickFunctionalComponentCtx(__VLS_10, __VLS_12)!;
let __VLS_14!: __VLS_NormalizeEmits<typeof __VLS_13.emit>;
{
const __VLS_15 = ({} as 'VRow' extends keyof typeof __VLS_ctx ? { 'VRow': typeof __VLS_ctx.VRow; } : 'vRow' extends keyof typeof __VLS_ctx ? { 'VRow': typeof __VLS_ctx.vRow; } : 'v-row' extends keyof typeof __VLS_ctx ? { 'VRow': (typeof __VLS_ctx)["v-row"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VRow;
const __VLS_16 = __VLS_asFunctionalComponent(__VLS_15, new __VLS_15({ ...{}, }));
({} as { VRow: typeof __VLS_15; }).VRow;
({} as { VRow: typeof __VLS_15; }).VRow;
const __VLS_17 = __VLS_16({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_16));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_15, typeof __VLS_17> & Record<string, unknown>) => void)({ ...{}, });
const __VLS_18 = __VLS_pickFunctionalComponentCtx(__VLS_15, __VLS_17)!;
let __VLS_19!: __VLS_NormalizeEmits<typeof __VLS_18.emit>;
{
const __VLS_20 = ({} as 'VCol' extends keyof typeof __VLS_ctx ? { 'VCol': typeof __VLS_ctx.VCol; } : 'vCol' extends keyof typeof __VLS_ctx ? { 'VCol': typeof __VLS_ctx.vCol; } : 'v-col' extends keyof typeof __VLS_ctx ? { 'VCol': (typeof __VLS_ctx)["v-col"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VCol;
const __VLS_21 = __VLS_asFunctionalComponent(__VLS_20, new __VLS_20({ ...{}, }));
({} as { VCol: typeof __VLS_20; }).VCol;
({} as { VCol: typeof __VLS_20; }).VCol;
const __VLS_22 = __VLS_21({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_21));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_20, typeof __VLS_22> & Record<string, unknown>) => void)({ ...{}, });
const __VLS_23 = __VLS_pickFunctionalComponentCtx(__VLS_20, __VLS_22)!;
let __VLS_24!: __VLS_NormalizeEmits<typeof __VLS_23.emit>;
{
const __VLS_25 = ({} as 'VCard' extends keyof typeof __VLS_ctx ? { 'VCard': typeof __VLS_ctx.VCard; } : 'vCard' extends keyof typeof __VLS_ctx ? { 'VCard': typeof __VLS_ctx.vCard; } : 'v-card' extends keyof typeof __VLS_ctx ? { 'VCard': (typeof __VLS_ctx)["v-card"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VCard;
const __VLS_26 = __VLS_asFunctionalComponent(__VLS_25, new __VLS_25({ ...{}, class: ("blur-card mt-16 my-16"), width: ("400"), height: ("450"), }));
({} as { VCard: typeof __VLS_25; }).VCard;
({} as { VCard: typeof __VLS_25; }).VCard;
const __VLS_27 = __VLS_26({ ...{}, class: ("blur-card mt-16 my-16"), width: ("400"), height: ("450"), }, ...__VLS_functionalComponentArgsRest(__VLS_26));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_25, typeof __VLS_27> & Record<string, unknown>) => void)({ ...{}, class: ("blur-card mt-16 my-16"), width: ("400"), height: ("450"), });
const __VLS_28 = __VLS_pickFunctionalComponentCtx(__VLS_25, __VLS_27)!;
let __VLS_29!: __VLS_NormalizeEmits<typeof __VLS_28.emit>;
{
const __VLS_30 = ({} as 'VCardTitle' extends keyof typeof __VLS_ctx ? { 'VCardTitle': typeof __VLS_ctx.VCardTitle; } : 'vCardTitle' extends keyof typeof __VLS_ctx ? { 'VCardTitle': typeof __VLS_ctx.vCardTitle; } : 'v-card-title' extends keyof typeof __VLS_ctx ? { 'VCardTitle': (typeof __VLS_ctx)["v-card-title"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VCardTitle;
const __VLS_31 = __VLS_asFunctionalComponent(__VLS_30, new __VLS_30({ ...{}, class: ("text-center text-h3 font-weight-bold mx-auto ma-4"), }));
({} as { VCardTitle: typeof __VLS_30; }).VCardTitle;
({} as { VCardTitle: typeof __VLS_30; }).VCardTitle;
const __VLS_32 = __VLS_31({ ...{}, class: ("text-center text-h3 font-weight-bold mx-auto ma-4"), }, ...__VLS_functionalComponentArgsRest(__VLS_31));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_30, typeof __VLS_32> & Record<string, unknown>) => void)({ ...{}, class: ("text-center text-h3 font-weight-bold mx-auto ma-4"), });
const __VLS_33 = __VLS_pickFunctionalComponentCtx(__VLS_30, __VLS_32)!;
let __VLS_34!: __VLS_NormalizeEmits<typeof __VLS_33.emit>;
{
const __VLS_35 = __VLS_intrinsicElements["h5"];
const __VLS_36 = __VLS_elementAsFunctionalComponent(__VLS_35);
const __VLS_37 = __VLS_36({ ...{}, class: ("sri"), }, ...__VLS_functionalComponentArgsRest(__VLS_36));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_35, typeof __VLS_37> & Record<string, unknown>) => void)({ ...{}, class: ("sri"), });
const __VLS_38 = __VLS_pickFunctionalComponentCtx(__VLS_35, __VLS_37)!;
let __VLS_39!: __VLS_NormalizeEmits<typeof __VLS_38.emit>;
(__VLS_38.slots!).default;
}
(__VLS_33.slots!).default;
}
{
const __VLS_40 = ({} as 'VCardText' extends keyof typeof __VLS_ctx ? { 'VCardText': typeof __VLS_ctx.VCardText; } : 'vCardText' extends keyof typeof __VLS_ctx ? { 'VCardText': typeof __VLS_ctx.vCardText; } : 'v-card-text' extends keyof typeof __VLS_ctx ? { 'VCardText': (typeof __VLS_ctx)["v-card-text"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VCardText;
const __VLS_41 = __VLS_asFunctionalComponent(__VLS_40, new __VLS_40({ ...{}, }));
({} as { VCardText: typeof __VLS_40; }).VCardText;
({} as { VCardText: typeof __VLS_40; }).VCardText;
const __VLS_42 = __VLS_41({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_41));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_40, typeof __VLS_42> & Record<string, unknown>) => void)({ ...{}, });
const __VLS_43 = __VLS_pickFunctionalComponentCtx(__VLS_40, __VLS_42)!;
let __VLS_44!: __VLS_NormalizeEmits<typeof __VLS_43.emit>;
{
const __VLS_45 = ({} as 'VForm' extends keyof typeof __VLS_ctx ? { 'VForm': typeof __VLS_ctx.VForm; } : 'vForm' extends keyof typeof __VLS_ctx ? { 'VForm': typeof __VLS_ctx.vForm; } : 'v-form' extends keyof typeof __VLS_ctx ? { 'VForm': (typeof __VLS_ctx)["v-form"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VForm;
const __VLS_46 = __VLS_asFunctionalComponent(__VLS_45, new __VLS_45({ ...{}, ref: ("form"), modelValue: ((__VLS_ctx.valid)), }));
({} as { VForm: typeof __VLS_45; }).VForm;
({} as { VForm: typeof __VLS_45; }).VForm;
const __VLS_47 = __VLS_46({ ...{}, ref: ("form"), modelValue: ((__VLS_ctx.valid)), }, ...__VLS_functionalComponentArgsRest(__VLS_46));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_45, typeof __VLS_47> & Record<string, unknown>) => void)({ ...{}, ref: ("form"), modelValue: ((__VLS_ctx.valid)), });
const __VLS_48 = __VLS_pickFunctionalComponentCtx(__VLS_45, __VLS_47)!;
let __VLS_49!: __VLS_NormalizeEmits<typeof __VLS_48.emit>;
// @ts-ignore
(__VLS_ctx.form);
{
const __VLS_50 = ({} as 'VTextField' extends keyof typeof __VLS_ctx ? { 'VTextField': typeof __VLS_ctx.VTextField; } : 'vTextField' extends keyof typeof __VLS_ctx ? { 'VTextField': typeof __VLS_ctx.vTextField; } : 'v-text-field' extends keyof typeof __VLS_ctx ? { 'VTextField': (typeof __VLS_ctx)["v-text-field"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VTextField;
const __VLS_51 = __VLS_asFunctionalComponent(__VLS_50, new __VLS_50({ ...{}, class: ("mx-auto "), label: ("Username"), clearable: (true), prependIcon: ((__VLS_ctx.mdiAccount)), variant: ("outlined"), rules: (([(v) => !!v || 'Username is required'])), required: (true), }));
({} as { VTextField: typeof __VLS_50; }).VTextField;
({} as { VTextField: typeof __VLS_50; }).VTextField;
const __VLS_52 = __VLS_51({ ...{}, class: ("mx-auto "), label: ("Username"), clearable: (true), prependIcon: ((__VLS_ctx.mdiAccount)), variant: ("outlined"), rules: (([(v) => !!v || 'Username is required'])), required: (true), }, ...__VLS_functionalComponentArgsRest(__VLS_51));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_50, typeof __VLS_52> & Record<string, unknown>) => void)({ ...{}, class: ("mx-auto "), label: ("Username"), clearable: (true), prependIcon: ((__VLS_ctx.mdiAccount)), variant: ("outlined"), rules: (([(v) => !!v || 'Username is required'])), required: (true), });
const __VLS_53 = __VLS_pickFunctionalComponentCtx(__VLS_50, __VLS_52)!;
let __VLS_54!: __VLS_NormalizeEmits<typeof __VLS_53.emit>;
}
{
const __VLS_55 = ({} as 'VTextField' extends keyof typeof __VLS_ctx ? { 'VTextField': typeof __VLS_ctx.VTextField; } : 'vTextField' extends keyof typeof __VLS_ctx ? { 'VTextField': typeof __VLS_ctx.vTextField; } : 'v-text-field' extends keyof typeof __VLS_ctx ? { 'VTextField': (typeof __VLS_ctx)["v-text-field"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VTextField;
const __VLS_56 = __VLS_asFunctionalComponent(__VLS_55, new __VLS_55({ ...{}, class: ("mx-auto "), label: ("Password"), clearable: (true), prependIcon: ((__VLS_ctx.mdiLock)), variant: ("outlined"), type: ("password"), rules: (([(v) => !!v || 'Password is required'])), required: (true), }));
({} as { VTextField: typeof __VLS_55; }).VTextField;
({} as { VTextField: typeof __VLS_55; }).VTextField;
const __VLS_57 = __VLS_56({ ...{}, class: ("mx-auto "), label: ("Password"), clearable: (true), prependIcon: ((__VLS_ctx.mdiLock)), variant: ("outlined"), type: ("password"), rules: (([(v) => !!v || 'Password is required'])), required: (true), }, ...__VLS_functionalComponentArgsRest(__VLS_56));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_55, typeof __VLS_57> & Record<string, unknown>) => void)({ ...{}, class: ("mx-auto "), label: ("Password"), clearable: (true), prependIcon: ((__VLS_ctx.mdiLock)), variant: ("outlined"), type: ("password"), rules: (([(v) => !!v || 'Password is required'])), required: (true), });
const __VLS_58 = __VLS_pickFunctionalComponentCtx(__VLS_55, __VLS_57)!;
let __VLS_59!: __VLS_NormalizeEmits<typeof __VLS_58.emit>;
}
{
const __VLS_60 = ({} as 'VCardActions' extends keyof typeof __VLS_ctx ? { 'VCardActions': typeof __VLS_ctx.VCardActions; } : 'vCardActions' extends keyof typeof __VLS_ctx ? { 'VCardActions': typeof __VLS_ctx.vCardActions; } : 'v-card-actions' extends keyof typeof __VLS_ctx ? { 'VCardActions': (typeof __VLS_ctx)["v-card-actions"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VCardActions;
const __VLS_61 = __VLS_asFunctionalComponent(__VLS_60, new __VLS_60({ ...{}, class: ("justify-center mt-4"), }));
({} as { VCardActions: typeof __VLS_60; }).VCardActions;
({} as { VCardActions: typeof __VLS_60; }).VCardActions;
const __VLS_62 = __VLS_61({ ...{}, class: ("justify-center mt-4"), }, ...__VLS_functionalComponentArgsRest(__VLS_61));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_60, typeof __VLS_62> & Record<string, unknown>) => void)({ ...{}, class: ("justify-center mt-4"), });
const __VLS_63 = __VLS_pickFunctionalComponentCtx(__VLS_60, __VLS_62)!;
let __VLS_64!: __VLS_NormalizeEmits<typeof __VLS_63.emit>;
{
const __VLS_65 = ({} as 'VBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.VBtn; } : 'vBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.vBtn; } : 'v-btn' extends keyof typeof __VLS_ctx ? { 'VBtn': (typeof __VLS_ctx)["v-btn"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VBtn;
const __VLS_66 = __VLS_asFunctionalComponent(__VLS_65, new __VLS_65({ ...{}, block: (""), ...(__VLS_ctx.props), text: ("Login"), width: ("30%"), color: ("#14A3C7"), minWidth: ("164"), variant: ("flat"), }));
({} as { VBtn: typeof __VLS_65; }).VBtn;
({} as { VBtn: typeof __VLS_65; }).VBtn;
const __VLS_67 = __VLS_66({ ...{}, block: (""), ...(__VLS_ctx.props), text: ("Login"), width: ("30%"), color: ("#14A3C7"), minWidth: ("164"), variant: ("flat"), }, ...__VLS_functionalComponentArgsRest(__VLS_66));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_65, typeof __VLS_67> & Record<string, unknown>) => void)({ ...{}, block: (""), ...(__VLS_ctx.props), text: ("Login"), width: ("30%"), color: ("#14A3C7"), minWidth: ("164"), variant: ("flat"), });
const __VLS_68 = __VLS_pickFunctionalComponentCtx(__VLS_65, __VLS_67)!;
let __VLS_69!: __VLS_NormalizeEmits<typeof __VLS_68.emit>;
}
{
const __VLS_70 = ({} as 'VCol' extends keyof typeof __VLS_ctx ? { 'VCol': typeof __VLS_ctx.VCol; } : 'vCol' extends keyof typeof __VLS_ctx ? { 'VCol': typeof __VLS_ctx.vCol; } : 'v-col' extends keyof typeof __VLS_ctx ? { 'VCol': (typeof __VLS_ctx)["v-col"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VCol;
const __VLS_71 = __VLS_asFunctionalComponent(__VLS_70, new __VLS_70({ ...{}, "<vDialog": (true), width: ("500"), }));
({} as { VCol: typeof __VLS_70; }).VCol;
const __VLS_72 = __VLS_71({ ...{}, "<vDialog": (true), width: ("500"), }, ...__VLS_functionalComponentArgsRest(__VLS_71));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_70, typeof __VLS_72> & Record<string, unknown>) => void)({ ...{}, "<vDialog": (true), width: ("500"), });
const __VLS_73 = __VLS_pickFunctionalComponentCtx(__VLS_70, __VLS_72)!;
let __VLS_74!: __VLS_NormalizeEmits<typeof __VLS_73.emit>;
{
const __VLS_75 = __VLS_intrinsicElements["template"];
const __VLS_76 = __VLS_elementAsFunctionalComponent(__VLS_75);
const __VLS_77 = __VLS_76({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_76));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_75, typeof __VLS_77> & Record<string, unknown>) => void)({ ...{}, });
{
const [{ props }] = __VLS_getSlotParams((__VLS_73.slots!).activator);
{
const __VLS_78 = ({} as 'VBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.VBtn; } : 'vBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.vBtn; } : 'v-btn' extends keyof typeof __VLS_ctx ? { 'VBtn': (typeof __VLS_ctx)["v-btn"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VBtn;
const __VLS_79 = __VLS_asFunctionalComponent(__VLS_78, new __VLS_78({ ...{}, block: (""), ...(props), text: ("Don't have any account?"), width: ("30%"), color: ("#14A3C7"), minWidth: ("164"), variant: ("flat"), }));
({} as { VBtn: typeof __VLS_78; }).VBtn;
({} as { VBtn: typeof __VLS_78; }).VBtn;
const __VLS_80 = __VLS_79({ ...{}, block: (""), ...(props), text: ("Don't have any account?"), width: ("30%"), color: ("#14A3C7"), minWidth: ("164"), variant: ("flat"), }, ...__VLS_functionalComponentArgsRest(__VLS_79));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_78, typeof __VLS_80> & Record<string, unknown>) => void)({ ...{}, block: (""), ...(props), text: ("Don't have any account?"), width: ("30%"), color: ("#14A3C7"), minWidth: ("164"), variant: ("flat"), });
const __VLS_81 = __VLS_pickFunctionalComponentCtx(__VLS_78, __VLS_80)!;
let __VLS_82!: __VLS_NormalizeEmits<typeof __VLS_81.emit>;
}
// @ts-ignore
[valid, valid, valid, form, mdiAccount, mdiAccount, mdiAccount, mdiLock, mdiLock, mdiLock, props, props, props,];
}
}
{
const __VLS_83 = __VLS_intrinsicElements["template"];
const __VLS_84 = __VLS_elementAsFunctionalComponent(__VLS_83);
const __VLS_85 = __VLS_84({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_84));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_83, typeof __VLS_85> & Record<string, unknown>) => void)({ ...{}, });
{
const [{ isActive }] = __VLS_getSlotParams((__VLS_73.slots!).default);
{
const __VLS_86 = ({} as 'VCard' extends keyof typeof __VLS_ctx ? { 'VCard': typeof __VLS_ctx.VCard; } : 'vCard' extends keyof typeof __VLS_ctx ? { 'VCard': typeof __VLS_ctx.vCard; } : 'v-card' extends keyof typeof __VLS_ctx ? { 'VCard': (typeof __VLS_ctx)["v-card"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VCard;
const __VLS_87 = __VLS_asFunctionalComponent(__VLS_86, new __VLS_86({ ...{}, title: ("need Spotifuuu Plan?"), }));
({} as { VCard: typeof __VLS_86; }).VCard;
({} as { VCard: typeof __VLS_86; }).VCard;
const __VLS_88 = __VLS_87({ ...{}, title: ("need Spotifuuu Plan?"), }, ...__VLS_functionalComponentArgsRest(__VLS_87));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_86, typeof __VLS_88> & Record<string, unknown>) => void)({ ...{}, title: ("need Spotifuuu Plan?"), });
const __VLS_89 = __VLS_pickFunctionalComponentCtx(__VLS_86, __VLS_88)!;
let __VLS_90!: __VLS_NormalizeEmits<typeof __VLS_89.emit>;
{
const __VLS_91 = ({} as 'VCardText' extends keyof typeof __VLS_ctx ? { 'VCardText': typeof __VLS_ctx.VCardText; } : 'vCardText' extends keyof typeof __VLS_ctx ? { 'VCardText': typeof __VLS_ctx.vCardText; } : 'v-card-text' extends keyof typeof __VLS_ctx ? { 'VCardText': (typeof __VLS_ctx)["v-card-text"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VCardText;
const __VLS_92 = __VLS_asFunctionalComponent(__VLS_91, new __VLS_91({ ...{}, }));
({} as { VCardText: typeof __VLS_91; }).VCardText;
({} as { VCardText: typeof __VLS_91; }).VCardText;
const __VLS_93 = __VLS_92({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_92));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_91, typeof __VLS_93> & Record<string, unknown>) => void)({ ...{}, });
const __VLS_94 = __VLS_pickFunctionalComponentCtx(__VLS_91, __VLS_93)!;
let __VLS_95!: __VLS_NormalizeEmits<typeof __VLS_94.emit>;
(__VLS_94.slots!).default;
}
{
const __VLS_96 = ({} as 'VCardActions' extends keyof typeof __VLS_ctx ? { 'VCardActions': typeof __VLS_ctx.VCardActions; } : 'vCardActions' extends keyof typeof __VLS_ctx ? { 'VCardActions': typeof __VLS_ctx.vCardActions; } : 'v-card-actions' extends keyof typeof __VLS_ctx ? { 'VCardActions': (typeof __VLS_ctx)["v-card-actions"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VCardActions;
const __VLS_97 = __VLS_asFunctionalComponent(__VLS_96, new __VLS_96({ ...{}, }));
({} as { VCardActions: typeof __VLS_96; }).VCardActions;
({} as { VCardActions: typeof __VLS_96; }).VCardActions;
const __VLS_98 = __VLS_97({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_97));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_96, typeof __VLS_98> & Record<string, unknown>) => void)({ ...{}, });
const __VLS_99 = __VLS_pickFunctionalComponentCtx(__VLS_96, __VLS_98)!;
let __VLS_100!: __VLS_NormalizeEmits<typeof __VLS_99.emit>;
{
const __VLS_101 = ({} as 'VSpacer' extends keyof typeof __VLS_ctx ? { 'VSpacer': typeof __VLS_ctx.VSpacer; } : 'vSpacer' extends keyof typeof __VLS_ctx ? { 'VSpacer': typeof __VLS_ctx.vSpacer; } : 'v-spacer' extends keyof typeof __VLS_ctx ? { 'VSpacer': (typeof __VLS_ctx)["v-spacer"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VSpacer;
const __VLS_102 = __VLS_asFunctionalComponent(__VLS_101, new __VLS_101({ ...{}, }));
({} as { VSpacer: typeof __VLS_101; }).VSpacer;
({} as { VSpacer: typeof __VLS_101; }).VSpacer;
const __VLS_103 = __VLS_102({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_102));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_101, typeof __VLS_103> & Record<string, unknown>) => void)({ ...{}, });
const __VLS_104 = __VLS_pickFunctionalComponentCtx(__VLS_101, __VLS_103)!;
let __VLS_105!: __VLS_NormalizeEmits<typeof __VLS_104.emit>;
}
{
const __VLS_106 = ({} as 'VBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.VBtn; } : 'vBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.vBtn; } : 'v-btn' extends keyof typeof __VLS_ctx ? { 'VBtn': (typeof __VLS_ctx)["v-btn"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VBtn;
const __VLS_107 = __VLS_asFunctionalComponent(__VLS_106, new __VLS_106({ ...{ onClick: {} as any, }, color: ("#d71e61"), variant: ("tonal"), text: ("Close "), }));
({} as { VBtn: typeof __VLS_106; }).VBtn;
({} as { VBtn: typeof __VLS_106; }).VBtn;
const __VLS_108 = __VLS_107({ ...{ onClick: {} as any, }, color: ("#d71e61"), variant: ("tonal"), text: ("Close "), }, ...__VLS_functionalComponentArgsRest(__VLS_107));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_106, typeof __VLS_108> & Record<string, unknown>) => void)({ ...{ onClick: {} as any, }, color: ("#d71e61"), variant: ("tonal"), text: ("Close "), });
const __VLS_109 = __VLS_pickFunctionalComponentCtx(__VLS_106, __VLS_108)!;
let __VLS_110!: __VLS_NormalizeEmits<typeof __VLS_109.emit>;
let __VLS_111 = { 'click': __VLS_pickEvent(__VLS_110['click'], ({} as __VLS_FunctionalComponentProps<typeof __VLS_107, typeof __VLS_108>).onClick) };
__VLS_111 = {
click: $event => {
isActive.value = false;
}
};
}
{
const __VLS_112 = ({} as 'VBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.VBtn; } : 'vBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.vBtn; } : 'v-btn' extends keyof typeof __VLS_ctx ? { 'VBtn': (typeof __VLS_ctx)["v-btn"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VBtn;
const __VLS_113 = __VLS_asFunctionalComponent(__VLS_112, new __VLS_112({ ...{ onClick: {} as any, }, color: ("#1e7ed7"), variant: ("tonal"), to: ("/package"), text: ("Getstarted"), }));
({} as { VBtn: typeof __VLS_112; }).VBtn;
({} as { VBtn: typeof __VLS_112; }).VBtn;
const __VLS_114 = __VLS_113({ ...{ onClick: {} as any, }, color: ("#1e7ed7"), variant: ("tonal"), to: ("/package"), text: ("Getstarted"), }, ...__VLS_functionalComponentArgsRest(__VLS_113));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_112, typeof __VLS_114> & Record<string, unknown>) => void)({ ...{ onClick: {} as any, }, color: ("#1e7ed7"), variant: ("tonal"), to: ("/package"), text: ("Getstarted"), });
const __VLS_115 = __VLS_pickFunctionalComponentCtx(__VLS_112, __VLS_114)!;
let __VLS_116!: __VLS_NormalizeEmits<typeof __VLS_115.emit>;
let __VLS_117 = { 'click': __VLS_pickEvent(__VLS_116['click'], ({} as __VLS_FunctionalComponentProps<typeof __VLS_113, typeof __VLS_114>).onClick) };
__VLS_117 = {
click: $event => {
isActive.value = false;
}
};
}
(__VLS_99.slots!).default;
}
(__VLS_89.slots!).default;
}
}
}
}
(__VLS_63.slots!).default;
}
(__VLS_48.slots!).default;
}
(__VLS_43.slots!).default;
}
(__VLS_28.slots!).default;
}
(__VLS_23.slots!).default;
}
(__VLS_18.slots!).default;
}
(__VLS_13.slots!).default;
}
(__VLS_8.slots!).default;
}
(__VLS_3.slots!).default;
}
if (typeof __VLS_styleScopedClasses === 'object' && !Array.isArray(__VLS_styleScopedClasses)) {
}
let __VLS_slots!: {};
return __VLS_slots;
}
