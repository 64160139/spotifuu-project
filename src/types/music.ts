export default interface Music {
  id: number
  musicImage?: string
  musicName: string
  musicLongtime: Date
  numPlay: number
}
