export default interface User {
  id ?: number

  name: string

  displayName: string

  password: string

  userImage?: string

  userEmail: string

  userRole: string
}
