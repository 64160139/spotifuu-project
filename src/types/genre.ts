export default interface Genre {
  id ?: number

  genreName : string

  genreImage : string
}
