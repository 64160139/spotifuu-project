import router from '@/router'
import userService from '@/service/userService'
import type User from '@/types/user'
import type { AxiosResponse } from 'axios'
import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useUserStore = defineStore('user', () => {
  const name = ref('')
  const displayName = ref('')
  const password = ref('')
  const userImage = ref('')
  const userRole = ref('')
  const userEmail = ref('')
  const isLogin = ref(false)
  const usersService = userService
  const user = ref<User[]>([])
  const userRouter = router
  function choosePlan(role: string) {
    userRole.value = role
    console.log(role)
  }

  const loginUser = async (loginName: string, loginpass: string) => {
    try {
      // เข้าการทำงาน เช็ค login
      const res = await usersService.getLogin(loginName, loginpass)
      console.log(res)
      const checkLogin = res.data as User
      if (checkLogin.name === loginName && checkLogin.password === loginpass) {
        isLogin.value = true // เข้าทำการ Login สำเร็จ
        name.value = loginName
        userRouter.push('/Listener') // ทำการ Link ไปหน้า Listener
      }
      // สิ้นสุดการทำงาน
    } catch (error) {
      console.log(error)
    }
  }
  const save = async (name: string, displayName: string, password: string, email: string) => {
    console.log(name)
    console.log(displayName)
    console.log(password)
    console.log(email)
    try {
      const userData = {
        name: name,
        displayName: displayName,
        password: password,
        userImage: '',
        userEmail: email,
        userRole: userRole.value // Accessing the .value property
      }
      console.log(userData)
      await userService.saveUsers(userData) // Now userData is defined before this line
      console.log('User data saved successfully')
    } catch (error) {
      console.error('Error while saving user data:', error)
    }
  }
  const getArtists = async () => {
    try {
      const res = await usersService.getUsers()
      const artists = res.data.filter((user: User) => user.userRole === 'Artist')
      return artists
    } catch (error) {
      console.error('Error while fetching artists:', error)
    }
  }
  return {
    // Return the .value property for ref objects
    name: name.value,
    displayName: displayName.value,
    password: password.value,
    userImage: userImage.value,
    userRole: userRole.value,
    userEmail: userEmail.value,
    save,
    choosePlan,
    loginUser,
    isLogin,
    user,
    getArtists
  }
})
function push(arg0: User) {
  throw new Error('Function not implemented.')
}
