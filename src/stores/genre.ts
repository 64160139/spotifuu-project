import { ref } from 'vue'
import { defineStore } from 'pinia'
import genreService from '@/service/genreService'
import type Genre from '@/types/genre'

export const useGenreStore = defineStore('genre', () => {
  const listGenres = ref<Genre[]>([])

  const getGenres = async () => {
    try {
      const res = await genreService.getGenres()
      const genres = res.data as Genre[]
      console.log(genres)
      return genres;
    } catch (error) {
      console.log(error)
    }
  }

  return { getGenres }
})
