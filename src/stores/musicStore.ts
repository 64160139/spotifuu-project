import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import musicService from '@/service/musicService'
import type Music from '@/types/music'

export const useMusicStore = defineStore('music', () => {
  const listMucis = ref<Music[]>([])
  const dialog = ref(false);
  const musicName = ref("");
  const getMusicss = async () => {
    try {
      const res = await musicService.getMusics()
      const musics = res.data as Music
      listMucis.value.push(musics)
      console.log(listMucis)
    } catch (error) {
      console.log(error)
    }
  }

  const getMusicByName = async (name:string) => {
    try {
      const res = await musicService.findByName(name);
      const music = res.data as Music
      return music.musicName
    } catch (error) {
      console.log(error)
      return null;
    }
  }

  const getTop10Counts = async () => {
    try {
      // Assuming musicService.getMusics() returns an array of music objects
      const res = await musicService.getMusics();
      const musics = res.data as Music[];
      // Sort the music array by the count in descending order
      musics.sort((a, b) => b.numPlay - a.numPlay);
      // Extract only the top 10 songs
      const top10Songs = musics.slice(0, 10).map(music => ({
        id: music.id,
        musicImage: music.musicImage,
        musicName: music.musicName,
        musicLongtime: music.musicLongtime,
        numPlay: music.numPlay
      }));
      console.log(top10Songs)
      return top10Songs;
    } catch (error) {
      console.log(error);
    }
  };

  
  return { getMusicss,dialog , getMusicByName , getTop10Counts }
})
