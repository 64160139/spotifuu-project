import type musicStore from '@/types/music'

import http from './axios'
import type Music from '@/types/music'
function getMusics() {
  return http.get('/musics') // ดึงเพลงทั้งหมดออกมา
}

function addMusic(music: Music) {
  // Add เพลง
  return http.post('/musics/', music)
}
function editMusic(id: number, music: Music) {
  // แก้ไขเพลงนั้นๆ ทั้งตาม ID
  return http.patch(`/musics/${id}`, music)
}
function removeMusic(id: number) {
  // ลบเพลงนั้นๆ ทั้งตาม ID
  return http.delete(`/musics/${id}`)
}

function findByName(musicName: string) {
  return http.post('/musics/findName',  { musicName }  )
}
export default { getMusics, addMusic, editMusic, removeMusic, findByName } // ส่ง function ออกไปใช้
