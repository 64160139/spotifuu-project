import type User from '@/types/user'
import http from './axios'
function getUsers() {
  // ดึงข้อมูล Users ทั้งหมดออกมา
  return http.get('/users')
}
function saveUsers(user: User) {
  // Add User
  return http.post('/users', user)
}
function editUsers(id: number, user: User) {
  // แก้ไข User
  return http.patch(`/users/${id}`, user)
}
function deleteUser(id: number) {
  // ลบ User
  return http.delete(`/users/${id}`)
}
function getLogin(name: string, password: string) {
  return http.post('/users/login', { name, password });
}
export default { getUsers, saveUsers, editUsers, deleteUser, getLogin } // ส่ง function ออกไปใช้
