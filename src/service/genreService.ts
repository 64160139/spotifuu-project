import http from './axios'
import type Genre from '@/types/genre' // Assuming you have a Genre type

function getGenres() {
  return http.get('/genres')
}

function saveGenre(genre: Genre) {
  return http.post('/genres', genre)
}

export default { getGenres, saveGenre }
