import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ListenerView from '@/views/ListenerView.vue'
import UnknownView from '@/views/UnknownView.vue'
import ArtistView from '@/views/Artist/ArtistView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'Login',
      // component: () => import('../views/HomeView.vue'),
      components: {
        default : HomeView,
      },
      meta: {
        layout: "FullLayout",
        // requiresAuth: true,
      },
    },

    {
      path: '/',
      name: 'First',
      // component: () => UnknownViewVue
      components: {
        default : UnknownView,
        header: () => import('../components/Unknown/UnknowHeader.vue')
      },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },

    {
      path: '/listener',
      name: 'Listener',
      components: {
        default : ListenerView,
        player: () => import('@/components/Listener/MusicPlayerNbtn.vue'),
        header: () => import('@/components/Listener/ListenerHeader.vue'),
      },
      meta: {
        layout: "PlayerLayout",
        // requiresAuth: true,
      },
    },
 
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },

    {
      path: '/register',
      name: 'RegisterView',
      components: {
        default : () => import('@/views/RegisterView.vue'),
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: '/package',
      name: 'PackageView',
      components: {
        default : () => import('../views/PackageView.vue'),
      },
      meta: {
        layout: "FullLayout",
      },
    },

    {
      path: '/payment',
      name: 'PaymentView',
      components: {
        default : () => import('../views/PaymentView.vue'),
      },
      meta: {
        layout: "FullLayout",
      },
    },

    {
      path: '/success',
      name: 'SuccessView',
      components: {
        default : () => import('../views/SuccessLoginView.vue'),
      },
      meta: {
        layout: "FullLayout",
      },
    },

    {
      path: '/unknow/search',
      name: 'UnknowSearchView',
      components: {
        default : () => import('../views/Unknown/UnknownSearch.vue'),
        player: () => import('@/components/Listener/MusicPlayerNbtn.vue'),
        header: () => import('@/components/Listener/ListenerHeader.vue'),
      },
      meta: {
        layout: "PlayerLayout",
        // requiresAuth: true,
      },
    },
    
    {
      path: '/listener/search',
      name: 'ListenerSearchView',
      components: {
        default : () => import('../views/Listener/ListenerSearch.vue'),
        player: () => import('@/components/Listener/MusicPlayerNbtn.vue'),
        header: () => import('@/components/Listener/ListenerHeader.vue'),
      },
      meta: {
        layout: "PlayerLayout",
        // requiresAuth: true,
      },
    },

    {
      path: '/listener/library',
      name: 'ListenerLibraryView',
      components: {
        default : () => import('../views/Listener/ListenerLibrary.vue'),
        player: () => import('@/components/Listener/MusicPlayerNbtn.vue'),
        header: () => import('@/components/Listener/ListenerHeader.vue'),
      },
      meta: {
        layout: "PlayerLayout",
        // requiresAuth: true,
      },
    },

    {
      path: '/artist',
      name: 'Dashboard',
      components: {
        default : ArtistView,
        header: () => import('../components/Artist/ArtistHead.vue')
      },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },

    {
      path: '/artist/song_management',
      name: 'Song Management',
      components: {
        default : () => import('@/views/Artist/SongManageView.vue'),
        header: () => import('../components/Artist/ArtistHead.vue')
      },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },

    {
      path: '/addsong',
      name: 'Add song',
      components: {
        default : () => import('@/views/Artist/AddsongView.vue')
      },
      meta: {
        layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: '/cansearch',
      name: 'ListenercanSearch',
      components: {
        default : () => import('@/views/Listener/ListenercanSearch.vue'),
        header: () => import('../components/Artist/ArtistHead.vue')
      },
      meta: {
        layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    


    // {
    //   path: '/player',
    //   name: 'Whatever',
    //   components: {
    //     default : () => import('@/components/Listener/MusicPlayerNbtn.vue'),
    //   },
    //   meta: {
    //     layout: "FullLayout",
    //   },

    // }

    // {
    //   path: '/player',
    //   name: 'MusicPlayer',
    //   components: {
    //     default : () => import('../views/MusicPlayer.vue'),
    //   },
    //   meta: {
    //     layout: "FullLayout",
    //   },
    // }
  ]
})

export default router
